//
//  NewSchoolViewController.swift
//  Most Awesome Programming Contest
//
//  Created by Randall Zane Porter on 3/14/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class NewSchoolViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var coachTF: UITextField!
    
    @IBAction func done(_ sender: Any) {
        if let name = nameTF.text, let coach = coachTF.text {
            if name != "" && coach != "" {
                Schools.shared?.add(school: School(name: name, coach: coach))
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
