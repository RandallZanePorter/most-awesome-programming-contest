//
//  StudentsViewController.swift
//  Most Awesome Programming Contest
//
//  Created by Randall Zane Porter on 3/15/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class StudentsViewController: UIViewController {
    
    var team:Team!

    override func viewDidLoad() {
        if team != nil {
            let students = team.students
            if students[0] != "" { stu0LBL.text = students[0] }
            if students[1] != "" { stu1LBL.text = students[1] }
            if students[2] != "" { stu2LBL.text = students[2] }
        }
        super.viewDidLoad()
    }

    @IBOutlet weak var stu0LBL: UILabel!
    @IBOutlet weak var stu1LBL: UILabel!
    @IBOutlet weak var stu2LBL: UILabel!
    
    override func viewDidAppear(_ animated: Bool) {
        if team != nil {
            let students = team.students
            if students[0] != "" { stu0LBL.text = students[0] }
            if students[1] != "" { stu1LBL.text = students[1] }
            if students[2] != "" { stu2LBL.text = students[2] }
        }
        super.viewDidAppear(animated)
    }
    
    @IBAction func exit(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
