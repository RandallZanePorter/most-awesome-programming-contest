//
//  SchoolsTableViewController.swift
//  Most Awesome Programming Contest
//
//  Created by Randall Zane Porter on 3/14/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class SchoolsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var schools = Schools.shared == nil ? Schools() : Schools.shared!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schools.numSchools()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "school")!
        cell.textLabel?.text = schools[indexPath.row].name
        cell.detailTextLabel?.text = schools[indexPath.row].coach
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "teams" {
            let teamTVC = segue.destination as! TeamsTableViewController
            teamTVC.school = schools[tableView.indexPathForSelectedRow!.row]
        }
    }

}
