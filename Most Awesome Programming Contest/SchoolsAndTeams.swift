//
//  SchoolsAndTeams.swift
//  Most Awesome Programming Contest
//
//  Created by Randall Zane Porter on 3/14/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import Foundation

class Schools {
    static var shared:Schools?
    var schools:[School] = []
    
    init (){
        self.schools = []
        Schools.shared = self
    }
    func numSchools() -> Int {
        return schools.count
    }
    func add(school:School) -> Void {
        schools.append(school)
    }
    func delete(school:School) -> Void {
        var remIndex = -1
        for i in 0..<numSchools() {
            if schools[i] == school {
                remIndex = i
            }
        }
        if remIndex != -1 {
            schools.remove(at: remIndex)
        }
        
    }
    func getSchools() -> [School] {
        return schools
    }
    subscript(index: Int) -> School {
        return schools[index]
    }
}

class School : Equatable {
    static func == (lhs: School, rhs: School) -> Bool {
        return lhs.name == rhs.name && lhs.coach == rhs.coach && lhs.teams == rhs.teams
    }
    
    var name:String
    var coach:String
    var teams:[Team]
    
    init (name:String, coach:String){
        self.name = name
        self.coach = coach
        self.teams = []
    }
    func addTeam(name:String, students:[String]){
        teams.append(Team(name: name, students:students))
    }
}

class Team: Equatable {
    static func == (lhs: Team, rhs: Team) -> Bool {
        return lhs.name == rhs.name && lhs.students == rhs.students
    }
    
    var name:String
    var students:[String]
    
    convenience init (name:String) {
        self.init(name: name, students: [])
    }
    init (name:String, students:[String]) {
        self.name = name
        self.students = students
    }
}
