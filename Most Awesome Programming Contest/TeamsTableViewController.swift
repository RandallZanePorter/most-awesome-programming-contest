//
//  TeamsTableViewController.swift
//  Most Awesome Programming Contest
//
//  Created by Randall Zane Porter on 3/14/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class TeamsTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    var school:School!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return school.teams.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "school")!
        cell.textLabel?.text = school.teams[indexPath.row].name
        return cell
    }
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidAppear(_ animated: Bool) {
        self.tableView.reloadData()
        super.viewDidAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func returnToSchools(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addTeam" {
            let teamVC = segue.destination as! NewTeamViewController
            teamVC.school = self.school
        } else if segue.identifier == "toStudents" {
            let studentsVC = segue.destination as! StudentsViewController
            studentsVC.team = school.teams[tableView.indexPathForSelectedRow!.row]
        }
    }
    
}
