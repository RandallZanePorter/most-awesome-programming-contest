//
//  NewTeamViewController.swift
//  Most Awesome Programming Contest
//
//  Created by Randall Zane Porter on 3/14/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class NewTeamViewController: UIViewController {
    
    var school:School!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var student0TF: UITextField!
    @IBOutlet weak var student1TF: UITextField!
    @IBOutlet weak var student2TF: UITextField!
    
    
    @IBAction func done(_ sender: Any) {
        if let name = nameTF.text,
           let stu0 = student0TF.text,
           let stu1 = student1TF.text,
           let stu2 = student2TF.text {
            if name != "" {
                school.addTeam(name: name, students: [stu0, stu1, stu2])
            }
        }
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}
